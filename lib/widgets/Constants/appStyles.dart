// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:workproject_1/widgets/Constants/appColors.dart';

class AppStyles {
  static const TextStyle nunito16w400 = TextStyle(
      fontSize: 16,
      fontWeight: FontWeight.w400,
      letterSpacing: -0.41,
      color: AppColors.grey
  );
  static const TextStyle nunito40w700 = TextStyle(
      fontSize: 40,
      fontWeight: FontWeight.w700,
      letterSpacing: -0.374,
      color: AppColors.black
  );
}
